@extends('layouts.app')
@section('content')
    <div class="welcome pt-3 pb-3" id="welcome">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="header-text-h1 pt-3">Newspaper - Classified Advertisments</h1>
                    <p class="white-text">This template is available for 100% free of charge on TemplateMo. Download, modify and use this for your business website.</p>
                    <button class="app-get-start-btn"><div class="btn-icon"><i class="fa fa-edit"></i></div> Create</button>
                </div>
                <div class="col-md-6">
                    <img src="{{asset('images/slider-icon.png')}}" alt="Image" class="rounded img-fluid">
                </div>
            </div>
        </div>
    </div>
   <div class="container mt-5">
        <div class="row pt-5 pb-5">
            <div class="col-md-6 d-none d-md-block">
                <img src="{{asset('images/news-success.png')}}" alt="Image" class="rounded img-fluid">
            </div>
            <div class="col-md-6">
                <div class="card border-0 shadow-sm mb-3">
                    <div class="card-body">
                        <div class="media">
                            <img src="{{asset('images/create.png')}}" class="mr-3" alt="...">
                            <div class="media-body">
                            <h5 class="mt-0">Create</h5>
                            <p class="app-subcontent-text m-0">This is my new text</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card border-0 shadow-sm mb-3">
                    <div class="card-body">
                        <div class="media">
                            <img src="{{asset('images/publish.png')}}" class="mr-3" alt="...">
                            <div class="media-body">
                            <h5 class="mt-0">Publish</h5>
                            <p class="app-subcontent-text m-0">This is my new text</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card border-0 shadow-sm mb-3">
                    <div class="card-body">
                        <div class="media">
                            <img src="{{asset('images/payonline.png')}}" class="mr-3" alt="...">
                            <div class="media-body">
                            <h5 class="mt-0">Pay Online</h5>
                            <p class="app-subcontent-text m-0">This is my new text</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
   <div class="white mb-5 pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-badge green text-white">Mobile App</div>
                    <h5 class="mt-3">Create Your Adv on Mobile App</h5>
                    <p class="app-subcontent-text">This is sample content to put here for showing i'm a content for reading user</p>
                    <button class="app-download-app"><img src="{{asset('images/android.png')}}" class="btn-icon" alt="Image">Android</button>
                    <button class="app-download-app"><img src="{{asset('images/ios.png')}}" class="btn-icon" alt="Image">Iphone</button>
                </div>
                <div class="col-md-5">
                    
                </div>
            </div>
        </div>
   </div>
@endsection