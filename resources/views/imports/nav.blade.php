<nav class="navbar navbar-expand-lg app-nav">
    <a class="navbar-brand white-text" href="#">MyNews</a>
    <div class="dropdown navbar-toggler">
        <a href="#" id="dropdownMenuButton" class="text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="{{asset('images/menu.png')}}" alt="Menu" height="25px" width="25px">
        </a>
        <div class="dropdown-menu border-0 shadow-sm dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            @guest
            <a class="dropdown-item" href="{{route('login')}}">Login</a>
            <a class="dropdown-item" href="{{route('register')}}">Sign Up</a>
            @endguest
            @auth
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <span>Logout</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                style="display: none;">
                @csrf
            </form>
            @endauth
        </div>
    </div>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            @guest
            <li class="nav-item">
            <a class="nav-link" href="{{route('login')}}">Login</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{route('register')}}">Sign Up</a>
            </li>
            @endguest
            @auth
            <li>
                <a  class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form-side').submit();">
                    <span>Logout</span>
                </a>
                <form id="logout-form-side" action="{{ route('logout') }}" method="POST"
                    style="display: none;">
                    @csrf
                </form>
            </li>
            @endauth
        </ul>
    </div>
</nav>