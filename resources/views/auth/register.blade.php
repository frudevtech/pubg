@extends('layouts.plain')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <div class="text-center mb-4">
              <img src="{{asset('images/publish.png')}}" alt="Logo" class="img-fluid align-center">
            </div>
            <h5 class="card-title text-center">Sign Up</h5>
            <form class="form-signin" method="POST" action="{{ route('register') }}">
              @csrf
              <div class="form-label-group">
                <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name"
                name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                <label for="name">Name</label>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              
              <div class="form-label-group">
                <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email address"
                 required name="email" value="{{ old('email') }}" autocomplete="email">
                <label for="email">Email address</label>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>

              <div class="form-label-group">
                <input type="password" id="inputPassword" class="form-control @error('password') is-invalid @enderror" placeholder="Password"
                 required autocomplete="new-password" name="password">
                <label for="inputPassword">Password</label>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-label-group">
                <input type="password" id="password_confirmation" class="form-control" placeholder="password-confirm"
                 required autocomplete="new-password-confirm" name="password_confirmation">
                <label for="password_confirmation">Confirm Password</label>
              </div>
              <button class="btn btn-lg btn-success btn-block text-uppercase" type="submit">Sign Up</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
