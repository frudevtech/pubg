@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <h4>Select Newspaper</h4>
    <div class="app-home-container">
        <div class="app-home-search">
            <div class="card border-0 shadow-sm">
                <div class="card-body grey lighten-5">
                    <div class="app-subcontent-text">Searching for...</div>
                    <input type="text" class="form-control">
                </div>
            </div>
        </div>
        <div class="app-home-filter">
            <div class="card border-0 shadow-sm">
                <div class="card-body">
                    <div class="font-weight-bold app-home-filter-header mb-2">Filters</div>
                    <hr>
                    <div class="form-group mb-1">
                        <input type="checkbox" name="offers" id="offers">
                        <label for="offers" class="app-home-lable">Offers</label>
                    </div>
                    <div class="form-group">
                        <label for="edition" class="app-subcontent-text">Edition</label>
                        <select name="edition" class="form-control" id="edition">
                            <option value="" disabled selected>Select</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="language" class="app-subcontent-text">Language</label>
                        <select name="language" class="form-control" id="language">
                            <option value="" disabled selected>Select</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="app-home-cards">
            <div class="row">
                <div class="col-md-6">
                    <div class="card border-0 shadow-sm mb-2">
                        <div class="card-body">
                            <div class="media">
                                <img src="{{asset('images/imgplaceholder.png')}}" class="mr-3 app-home-placeholder" alt="Image">
                                <div class="media-body">
                                  <h5 class="mt-0">Dinakaran</h5>
                                  This is our planet
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-0 shadow-sm mb-2">
                        <div class="card-body">
                            <div class="media">
                                <img src="{{asset('images/imgplaceholder.png')}}" class="mr-3 app-home-placeholder" alt="Image">
                                <div class="media-body">
                                  <h5 class="mt-0">Dinakaran</h5>
                                  This is our planet
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-0 shadow-sm mb-2">
                        <div class="card-body">
                            <div class="media">
                                <img src="{{asset('images/imgplaceholder.png')}}" class="mr-3 app-home-placeholder" alt="Image">
                                <div class="media-body">
                                  <h5 class="mt-0">Dinakaran</h5>
                                  This is our planet
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card border-0 shadow-sm mb-2">
                        <div class="card-body">
                            <div class="media">
                                <img src="{{asset('images/imgplaceholder.png')}}" class="mr-3 app-home-placeholder" alt="Image">
                                <div class="media-body">
                                  <h5 class="mt-0">Dinakaran</h5>
                                  This is our planet
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection