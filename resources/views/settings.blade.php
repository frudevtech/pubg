@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/select2.css')}}">
<script src="{{asset('js/select2.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/jqui.css')}}">
<script src="{{asset('js/jqui.js')}}"></script>
<div class="container mt-5">
    <div class="media">
        <img src="{{asset('images/imgplaceholder.png')}}" class="mr-3 app-setting-placeholder" alt="Image">
        <div class="media-body">
          <h4 class="mt-0">Dinakaran</h4>
        </div>
    </div>
    <hr>
    <div class="card border-0 shadow-sm">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 mb-2">
                    <label for="language">Edition</label>
                    <select name="language" id="language" class="form-control">
                        <option value="" disabled>Select</option>
                    </select>
                </div>
                <div class="col-md-6 mb-2">
                    <label for="category">Category</label>
                    <select name="category" id="category" class="form-control">
                        <option value="" disabled>Select</option>
                    </select>
                </div>
                <div class="col-md-6 mb-2">
                    <label for="date">Publish Date</label>
                    <input type="text" name="date" id="date" class="form-control" readonly>
                </div>
                <div class="col-md-6 mb-2" style="line-height:90px;">
                    <button class="btn btn-sm btn-success mr-2">Classified Text</button>
                    <button class="btn btn-sm btn-info">Classified Display</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#language").select2();
        $("#category").select2();
        $("#date").datepicker({
            maxDate:"+15",
            minDate:"1"
        });
    });
</script>
@endsection