@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/select2.css')}}">
<script src="{{asset('js/select2.js')}}"></script>
<script src="{{asset('js/vue.js')}}"></script>
<div class="container mt-5">
    <div class="media">
        <img src="{{asset('images/imgplaceholder.png')}}" class="mr-3 app-setting-placeholder" alt="Image">
        <div class="media-body">
          <h4 class="mt-0">Dinakaran</h4>
        </div>
    </div>
    <hr>
    <div class="card shadow-sm border-0">
        <div class="card-body">
            <div class="row m-0" id="app">
                <div class="col col-md-4">
                    <h4 class="text-danger">Preview</h4>
                    <div id="prevtext" :class="'adv-preview-container bg-white'+(border?' border-dark':'')">
                        <span id="tm" v-if="tickmark">&#10004;</span><span id="atxt">@{{advtext}}</span>
                    </div>
                </div>
                <div class="col col-md-6">
                    <h4 class="h-b-b-danger">Compose Adv</h4>
                    <ul class="ks-cboxtags m-0 p-0">
                        <li><input type="checkbox" v-model="border" name="border" id="border" value="border"><label for="border">Border</label></li>
                        <li><input type="checkbox" v-model="tickmark" name="tickmark" id="tickmark" value="tickmark"><label for="tickmark">Tickmark</label></li>
                    </ul>
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <b>Char count : </b> <span id="cc">@{{  advtext.trim().length  }}</span>
                        </div>
                        <div class="col-md-12">
                            <b>Word count : </b> <span id="wc">@{{  advtext.trim()==""?0:advtext.trim().split(" ").length  }}</span>
                        </div>
                    </div>
                    <div class="form-group m-t-sm">
                        <label for="ui">Advertisment text</label>
                        <textarea required name="ui" id="ui" rows="4" v-model="advtext" class="form-control"></textarea>
                    </div>
                    <button type="submit" class="btn btn-success btn-sm">Create</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        let app = new Vue({
            el:"#app",
            components:{
                "date-time":{
                    template:"<input/>",
                    mounted:function(){
                        let self = this;
                        let currentDate = new Date(); 
                        $(this.$el).datepicker({dateFormat:"yy-mm-dd"});
                        $(this.$el).datepicker("setDate",currentDate);
                    }
                },
                "select2":{
                    template:"<select/>",
                    props:['datas'],
                    mounted:function(){
                        let self = this;
                        let val = JSON.parse(JSON.stringify(self.datas));
                        $(this.$el).select2({data:val});
                    }
                }
            },
            data:{
                advtext:"",
                e:[],
                s:[],
                border:false,
                tickmark:false
            },
            methods: {
                
            },
        });
    });
</script>
@endsection