<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function setting()
    {
        return view('settings');
    }

    public function classifiedtext()
    {
        return view('classifiedtext');
    }
}
